# Copyright 2016-2020 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ import=setuptools has_bin=true multibuild=false blacklist="2 3.5 3.6" test=pytest ] \
    systemd-service

SUMMARY="Open-source home automation platform running on Python 3"
DESCRIPTION="
The goal of Home Assistant is to be able to track and control all devices at home and offer a
platform for automating control.
"
HOMEPAGE+=" https://home-assistant.io"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

# Need to figure out
RESTRICT="test"

DEPENDENCIES="
    build+run:
        group/${PN}
        user/${PN}
        dev-python/Jinja2[>=2.10.3][python_abis:*(-)?]
        dev-python/PyJWT[>=1.7.1][python_abis:*(-)?]
        dev-python/PyYAML[>=5.3][python_abis:*(-)?]
        dev-python/aiohttp[>=3.6.1][python_abis:*(-)?]
        dev-python/astral[>=2.1][python_abis:*(-)?]
        dev-python/async-timeout[>=3.0.1][python_abis:*(-)?]
        dev-python/attrs[>=19.3.0][python_abis:*(-)?]
        dev-python/bcrypt[>=3.1.7][python_abis:*(-)?]
        dev-python/certifi[>=2019.11.28][python_abis:*(-)?]
        dev-python/ciso8601[>=2.1.3][python_abis:*(-)?]
        dev-python/cryptography[>=2.8][python_abis:*(-)?]
        dev-python/importlib_metadata[>=1.5.0][python_abis:*(-)?]
        dev-python/pip[>=8.0.3][python_abis:*(-)?]
        dev-python/python-slugify[>=4.0.0][python_abis:*(-)?]
        dev-python/pytz[>=2019.3][python_abis:*(-)?]
        dev-python/requests[>=2.23.0][python_abis:*(-)?]
        dev-python/ruamel-yaml[>=0.15.100][python_abis:*(-)?]
        dev-python/voluptuous[>=0.11.7][python_abis:*(-)?]
        dev-python/voluptuous-serialize[>=2.3.0][python_abis:*(-)?]
    run:
        dev-python/PyQRCode[>=1.2.1][python_abis:*(-)?]
        dev-python/SQLAlchemy[>=1.3.13][python_abis:*(-)?]
        dev-python/aiohttp-cors[>=0.7.0][python_abis:*(-)?]
        dev-python/colorlog[>=4.0.2][python_abis:*(-)?]
        dev-python/defusedxml[>=0.6.0][python_abis:*(-)?]
        dev-python/distro[>=1.4.0][python_abis:*(-)?]
        dev-python/home-assistant-frontend[>=20200318.1][python_abis:*(-)?]
        dev-python/netdisco[>=2.6.0][python_abis:*(-)?]
        dev-python/pynacl[>=1.3.0][python_abis:*(-)?]
        dev-python/pyotp[>=2.3.0][python_abis:*(-)?]
        dev-python/xmltodict[>=0.12.0][python_abis:*(-)?]
        dev-python/zeroconf[>=0.24.5][python_abis:*(-)?]
    suggestion:
        app-speech/svox [[
            description = [ Required for Home Assistant component: tts.picotts ]
        ]]
        dev-python/PyChromecast[>=4.2.0][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: cast ]
        ]]
        dev-python/pyHS100[>=0.3.5][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: tplink ]
        ]]
        dev-python/beautifulsoup4[>=4.8.2][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: linksys_ap, scrape, sytadin ]
        ]]
        dev-python/getmac[>=0.8.1][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: braviatv, huawei_lte, kef, minecraft_server, nmap_tracker ]
        ]]
        dev-python/gstreamer-player[>=1.1.2][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: gstreamer ]
        ]]
        dev-python/jsonrpc-async[>=0.6][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: kodi ]
        ]]
        dev-python/jsonrpc-websocket[>=0.6][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: kodi ]
        ]]
        dev-python/limitlessled[>=1.1.3][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: limitlessled ]
        ]]
        dev-python/mutagen[>=1.43.0][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: tts ]
        ]]
        dev-python/onkyo-eiscp[>=1.2.7][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: onkyo ]
        ]]
        dev-python/paho-mqtt[>=1.5.0][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: mqtt, shiftr ]
        ]]
        dev-python/psutil[>=5.6.7][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: systemmonitor ]
        ]]
        dev-python/pyhomematic[>=0.1.65][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: homematic ]
        ]]
        dev-python/pylast[>=3.2.0][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: lastfm ]
        ]]
        dev-python/python-mpd2[>=1.0.0][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: mpd ]
        ]]
        dev-python/python-nmap[>=0.6.1][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: nmap_tracker ]
        ]]
        dev-python/pywebpush[>=1.9.2][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: html5 ]
        ]]
        dev-python/snapcast[>=2.0.10][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: snapcast ]
        ]]
        dev-python/spotipy[>=2.7.1][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: spotify ]
        ]]
        dev-python/wakeonlan[>=1.1.6][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: wake_on_lan, panasonic_viera ]
        ]]
        sys-apps/net-tools [[
            description = [ Required for Home Assistant component: nmap_tracker ]
        ]]
        net-misc/iputils [[
            description = [ Required for Home Assistant component: wake_on_lan ]
        ]]
        net-misc/youtube-dl[>=2019.09.01][python_abis:*(-)?] [[
            description = [ Required for Home Assistant component: media_extractor ]
        ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-0.107.7-pr32270.patch
)

src_prepare() {
    setup-py_src_prepare

    # can be more recent
    edo sed \
        -e 's:PyJWT==1.7.1:PyJWT>=1.7.1:g' \
        -e 's:PyNaCl==1.3.0:PyNaCl>=1.3.0:g' \
        -e 's:aiohttp==3.6.1:aiohttp>=3.6.1:g' \
        -e 's:astral==2.1:astral>=2.1:g' \
        -e 's:async_timeout==3.0.1:async_timeout>=3.0.1:g' \
        -e 's:attrs==19.3.0:attrs>=19.3.0:g' \
        -e 's:bcrypt==3.1.7:bcrypt>=3.1.7:g' \
        -e 's:ciso8601==2.1.3:ciso8601>=2.1.3:g' \
        -e 's:cryptography==2.8:cryptography>=2.8:g' \
        -e 's:importlib-metadata==1.5.0:importlib-metadata>=1.5.0:g' \
        -e 's:python-slugify==4.0.0:python-slugify>=4.0.0:g' \
        -e 's:pyyaml==5.3:pyyaml>=5.3:g' \
        -e 's:requests==2.23.0:requests>=2.23.0:g' \
        -e 's:ruamel.yaml==0.15.100:ruamel.yaml>=0.15.100:g' \
        -e 's:voluptuous==0.11.7:voluptuous>=0.11.7:g' \
        -e 's:voluptuous-serialize==2.3.0:voluptuous-serialize>=2.3.0:g' \
        -i homeassistant/package_constraints.txt \
        -i setup.py

    # remove hass-nabucasa dependency
    edo sed \
        -e '/hass-nabucasa/d' \
        -i homeassistant/package_constraints.txt

    # Core components
    # homeassistant.auth.mfa_modules.totp
    edo sed \
        -e 's:PyQRCode==1.2.1:PyQRCode>=1.2.1:g' \
        -i homeassistant/auth/mfa_modules/totp.py
    # homeassistant.components.recorder
    # homeassistant.components.sql
    edo sed \
        -e 's:sqlalchemy==1.3.13:sqlalchemy>=1.3.13:g' \
        -i homeassistant/package_constraints.txt \
        -i homeassistant/components/{recorder,sql}/manifest.json
    # homeassistant.components.emulated_hue
    # homeassistant.components.http
    edo sed \
        -e 's:aiohttp_cors==0.7.0:aiohttp_cors>=0.7.0:g' \
        -i homeassistant/package_constraints.txt \
        -i homeassistant/components/{emulated_hue,http}/manifest.json
    # homeassistant.scripts.check_config
    edo sed \
        -e 's:colorlog==4.0.2:colorlog>=4.0.2:g' \
        -i homeassistant/scripts/check_config.py
    # homeassistant.components.namecheapdns
    # homeassistant.components.ssdp
    # homeassistant.components.ohmconnect
    # homeassistant.components.ihc
    edo sed \
        -e 's:defusedxml==0.6.0:defusedxml>=0.6.0:g' \
        -i homeassistant/package_constraints.txt \
        -i homeassistant/components/{namecheapdns,ssdp,ohmconnect,ihc}/manifest.json
    # homeassistant.components.updater
    edo sed \
        -e 's:distro==1.4.0:distro>=1.4.0:g' \
        -i homeassistant/package_constraints.txt \
        -i homeassistant/components/updater/manifest.json
    # homeassistant.components.frontend
    edo sed \
        -e 's:home-assistant-frontend==20200318.1:home-assistant-frontend>=20200318.1:g' \
        -i homeassistant/package_constraints.txt \
        -i homeassistant/components/frontend/manifest.json
    # homeassistant.components.discovery
    # homeassistant.components.ssdp
    edo sed \
        -e 's:netdisco==2.6.0:netdisco>=2.6.0:g' \
        -i homeassistant/package_constraints.txt \
        -i homeassistant/components/{discovery,ssdp}/manifest.json
    # homeassistant.auth.mfa_modules.notify
    # homeassistant.auth.mfa_modules.totp
    # homeassistant.components.otp
    edo sed \
        -e 's:pyotp==2.3.0:pyotp>=2.3.0:g' \
        -i homeassistant/auth/mfa_modules/{notify,totp}.py \
        -i homeassistant/components/otp/manifest.json
    # homeassistant.components.bluesound
    # homeassistant.components.rest
    # homeassistant.components.startca
    # homeassistant.components.ted5000
    # homeassistant.components.yr
    # homeassistant.components.zestimate
    edo sed \
        -e 's:xmltodict==0.12.0:xmltodict>=0.12.0:g' \
        -i homeassistant/components/{bluesound,rest,startca,ted5000,yr,zestimate}/manifest.json
    # homeassistant.components.zeroconf
    edo sed \
        -e 's:zeroconf==0.24.5:zeroconf>=0.24.5:g' \
        -i homeassistant/package_constraints.txt \
        -i homeassistant/components/zeroconf/manifest.json

    # Additional components
    # homeassistant.components.cast
    edo sed \
        -e 's:pychromecast==4.2.0:pychromecast>=4.2.0:g' \
        -i homeassistant/components/cast/manifest.json
    # homeassistant.components.html5
    edo sed \
        -e 's:pywebpush==1.9.2:pywebpush>=1.9.2:g' \
        -i homeassistant/components/html5/manifest.json
    # homeassistant.components.tplink
    edo sed \
        -e 's:pyHS100==0.3.5:pyHS100>=0.3.5:g' \
        -i homeassistant/components/tplink/manifest.json
    # homeassistant.components.linksys_ap
    # homeassistant.components.scrape
    # homeassistant.components.sytadin
    edo sed \
        -e 's:beautifulsoup4==4.8.2:beautifulsoup4>=4.8.2:g' \
        -i homeassistant/components/scrape/manifest.json
    # homeassistant.components.braviatv
    # homeassistant.components.huawei_lte
    # homeassistant.components.kef
    # homeassistant.components.minecraft_server
    # homeassistant.components.nmap_tracker
    edo sed \
        -e 's:getmac==0.8.1:getmac==0.8.1:g' \
        -i homeassistant/components/{braviatv,huawei_lte,kef,minecraft_server,nmap_tracker}/manifest.json
    # homeassistant.components.gstreamer
    edo sed \
        -e 's:gstreamer-player==1.1.2:gstreamer-player>=1.1.2:g' \
        -i homeassistant/components/gstreamer/manifest.json
    # homeassistant.components.kodi
    edo sed \
        -e 's:jsonrpc-async==0.6:jsonrpc-async>=0.6:g' \
        -e 's:jsonrpc-websocket==0.6:jsonrpc-websocket>=0.6:g' \
        -i homeassistant/components/kodi/manifest.json
    # homeassistant.components.limitlessled
    edo sed \
        -e 's:limitlessled==1.1.3:limitlessled>=1.1.3:g' \
        -i homeassistant/components/limitlessled/manifest.json
    # homeassistant.components.tts
    edo sed \
        -e 's:mutagen==1.43.0:mutagen>=1.43.0:g' \
        -i homeassistant/components/tts/manifest.json
    # homeassistant.components.onkyo
    edo sed \
        -e 's:onkyo-eiscp==1.2.7:onkyo-eiscp>=1.2.7:g' \
        -i homeassistant/components/onkyo/manifest.json
    # homeassistant.components.mqtt
    # homeassistant.components.shiftr
    edo sed \
        -e 's:paho-mqtt==1.5.0:paho-mqtt>=1.5.0:g' \
        -i homeassistant/components/{mqtt,shiftr}/manifest.json
    # homeassistant.components.systemmonitor
    edo sed \
        -e 's:psutil==5.6.7:psutil>=5.6.7:g' \
        -i homeassistant/components/systemmonitor/manifest.json
    # homeassistant.components.homematic
    edo sed \
        -e 's:pyhomematic==0.1.65:pyhomematic>=0.1.65:g' \
        -i homeassistant/components/homematic/manifest.json
    # homeassistant.components.lastfm
    edo sed \
        -e 's:pylast==3.2.0:pylast>=3.2.0:g' \
        -i homeassistant/components/lastfm/manifest.json
    # homeassistant.components.mpd
    edo sed \
        -e 's:python-mpd2==1.0.0:python-mpd2>=1.0.0:g' \
        -i homeassistant/components/mpd/manifest.json
    # homeassistant.components.nmap_tracker
    edo sed \
        -e 's:python-nmap==0.6.1:python-nmap>=0.6.1:g' \
        -i homeassistant/components/nmap_tracker/manifest.json
    # homeassistant.components.snapcast
    edo sed \
        -e 's:snapcast==2.0.10:snapcast>=2.0.10:g' \
        -i homeassistant/components/snapcast/manifest.json
    # homeassistant.components.spotify
    edo sed \
        -e 's:spotipy==2.7.1:spotipy>=2.7.1:g' \
        -i homeassistant/components/spotify/manifest.json
    # homeassistant.components.panasonic_viera
    # homeassistant.components.samsungtv
    # homeassistant.components.wake_on_lan
    edo sed \
        -e 's:wakeonlan==1.1.6:wakeonlan>=1.1.6:g' \
        -i homeassistant/components/{panasonic_viera,wake_on_lan}/manifest.json
    # homeassistant.components.media_extractor
    edo sed \
        -e 's:youtube_dl==2020.03.08:youtube_dl>=2020.03.08:g' \
        -i homeassistant/components/media_extractor/manifest.json

    # homeassistant.components.cloud
    # remove due to unpackaged hass-nabucasa
    edo rm -rf "${IMAGE}"/components/cloud
}

src_install() {
    setup-py_src_install

    keepdir /var/lib/homeassistant
    edo chown homeassistant:homeassistant "${IMAGE}"/var/lib/homeassistant

    install_systemd_files
}

