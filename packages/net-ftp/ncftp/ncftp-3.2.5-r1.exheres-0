# Copyright 2007 Bryan Østergaard, Peter Tersløv Forsberg
# Distributed under the terms of the GNU General Public License v2

SUMMARY="NcFTP - Versatile ncurses based FTP client"
HOMEPAGE="http://www.${PN}.com/${PN}"
DOWNLOADS="ftp://ftp.${PN}.com/${PN}/${PNV}-src.tar.bz2"

REMOTE_IDS="freecode:${PN}"

UPSTREAM_CHANGELOG="${HOMEPAGE}/doc/changelog.html [[ lang = en ]]"

LICENCES="ClarifiedArtistic"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="socks5"

DEPENDENCIES="
    build+run:
        sys-libs/ncurses"

DEFAULT_SRC_CONFIGURE_PARAMS=( --hates={docdir,datarootdir} --enable-ipv6 --with-ncurses )
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( socks5 )
DEFAULT_SRC_INSTALL_EXTRA_DOCS=( doc/FIREWALLS_AND_PROXIES.txt doc/CHANGELOG.txt DONATE.txt )

src_prepare() {
    edo sed -i -e 's/"$DESTDIR"/"$(DESTDIR)"/p' \
        -e 's:"$(DESTDIR)$(SYSCONFDIR)"::' \
        "${WORK}"/Makefile.in
    edo sed -i '/AR="ar"/D' "${WORK}"/configure
}

src_configure() {
    # FIXME: configure wants to connect DNS, since fixing is
    # non-trivial turn sandboxing off for port 53 for now.
    esandbox allow_net --connect "inet:0.0.0.1@53"
    default
    esandbox disallow_net --connect "inet:0.0.0.1@53"
}

src_install() {
    default

    insinto /usr/share/doc/${PNVR}/html
    doins doc/html/*
}

