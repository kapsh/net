# Copyright 2012 Marc-Antoine Perennou <Marc-Antoine@Perennou.com>
# Distributed under the terms of the GNU General Public License v2

require github
require systemd-service

SUMMARY="Open source in-memory key-value store"
DESCRIPTION="
Redis is an open source, advanced key-value store. It is often referred to as a data structure
server since keys can contain strings, hashes, lists, sets and sorted sets.
"
HOMEPAGE+=" https://redis.io/"
DOWNLOADS="https://download.redis.io/releases/${PNV}.tar.gz"

LICENCES="redis"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    systemd
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

# TODO: bundled jemalloc and lua
DEPENDENCIES="
    build:
        virtual/pkg-config
    run:
        group/redis
        user/redis
    build+run:
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
        systemd? ( sys-apps/systemd )
    test:
        dev-lang/tcl
"

MAKE_PARAMS=(
    MALLOC=jemalloc
    BUILD_TLS=yes
)
DEFAULT_SRC_COMPILE_PARAMS=(
    ${MAKE_PARAMS[@]}
    AR=${AR}
    V=1
)
DEFAULT_SRC_TEST_PARAMS=(
    ${MAKE_PARAMS[@]}
)
DEFAULT_SRC_INSTALL_PARAMS=(
    ${MAKE_PARAMS[@]}
    PREFIX="${IMAGE}"/usr/$(exhost --target)
)

src_prepare() {
    default

    edo sed -i deps/Makefile \
        -e "s:./configure:& AR=${AR}:" \
        -e "s:(MAKE):& RANLIB=${RANLIB}:"
}

src_compile() {
    emake \
        "${DEFAULT_SRC_COMPILE_PARAMS[@]}" \
        BUILD_WITH_SYSTEMD=$(option systemd yes no)
}

src_test() {
    esandbox allow_net --connect inet:127.0.0.1@11111
    esandbox allow_net inet:127.0.0.1@11111
    esandbox allow_net --connect inet:127.0.0.1@20000-30000
    esandbox allow_net inet:127.0.0.1@20000-30000
    esandbox allow_net "inet:0.0.0.0@0"
    esandbox allow_net "unix:${WORK}/tests/tmp/server.*/socket"

    emake \
        "${DEFAULT_SRC_TEST_PARAMS[@]}" \
        BUILD_WITH_SYSTEMD=$(option systemd yes no) \
        test

    esandbox disallow_net "unix:${WORK}/tests/tmp/server.*/socket"
    esandbox disallow_net "inet:0.0.0.0@0"
    esandbox disallow_net --connect inet:127.0.0.1@11111
    esandbox disallow_net inet:127.0.0.1@11111
    esandbox disallow_net --connect inet:127.0.0.1@20000-30000
    esandbox disallow_net inet:127.0.0.1@20000-30000
}

src_install() {
    emake \
        "${DEFAULT_SRC_INSTALL_PARAMS[@]}" \
        BUILD_WITH_SYSTEMD=$(option systemd yes no) \
        install

    install_systemd_files

    insinto /etc
    doins redis.conf sentinel.conf
    diropts -m 0755 -o redis -g redis

    keepdir /var/db/redis/{,.rdb}
}

